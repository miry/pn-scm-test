Site Reliability Engineer Test
====================

Hello and thanks for taking the time to try this out.

The goal of this test is to assert (to some degree) your coding and architectural skills. You're given a simple problem so you can focus on showcasing development techniques.

This test should be written in Go language.

You're **encouraged** to use GoLang Standard library and **allowed** to use third party libraries, as long as you put them together yourself **without relying on a framework or microframework** to do it for you. An effective developer knows what to build and what to reuse, but also how his/her tools work. Be prepared to answer some questions about those libraries, like why you chose them and what other alternatives you're familiar with.

We need to deploy application and all related service to Kubernetes cluster. Feel free to use minikube for testing or any other solution to setup a Kubernetes cluster. For testing will be used Kubernetes on AWS and Minikube. Propose own multi host kubernetes cluster solution for demo. We are open to any suggestions.

As this is a code review process, please avoid adding generated code to the project. This makes our jobs as reviewers more difficult, as we can't review code you didn't write. This means avoiding committing vendor folders or similar, which generates thousands of lines of code in stub files.


Instructions
-----

1. Clone this repository.
2. Create a new branch called `dev`.
3. Create a pull request from your `dev` branch to the master branch. This PR should contain setup instructions for your application and a breakdown of the technologies & packages you chose to use, why you chose to use them, and the design decisions you made.
4. Reply to the thread you're having with our HR department telling them we can start reviewing your code

Task
----

Build and deploy web application to short urls https://en.wikipedia.org/wiki/URL_shortening.

#### Requirements

- The endpoint response **must** be `JSON` encoded.
- The endpoint response time **must** be lower than `100ms`.
- The application should be stateless, i.e. it is **not allowed** to cache on the application side.
- The unknown endpoints should return HTTP status code 404
- Log all HTTP requests.

##### Use Case #1 - create url

A user should be able to create a new short url.

_Endpoint_

`POST /url`

_Request params_

- `url` : string contains a valid url

Example request body:

```
{"url": "http://example.com"}
```

Example response:

```
{"url": "http://google.com", "short": "http://localhost:3000/gIld"}
```

_Response codes_

- 201 : Success created a new url and return as JSON.
- 422 : Invalid params. Should return JSON with error message.

##### Use Case #2 - redirect

A user should be able be redirected to full url

_Endpoint_

`GET *url`

_Response codes_

- 301 : Redirect to location if it exists
- 404 : Return HTTP status code 404 if there are no such url created

##### Use Case #3 - deployment

The goal of this phase: you provide simple instructions or automated script to create infrastructure for this application. The infrastructure should be scalable.
(internal) Help topics: https://kubernetes.io/docs/tutorials/kubernetes-basics/cluster-intro/.


##### Use Case #4 - reliability

Write script `bin/benchmark`or documentation how to test performance and reliability of the deployed application.

Evaluation Criteria
--------------

1. The problems are solved efficiently and effectively, the application works as expected.
2. The application is supplied with the setup scripts. Consider using minikube and a one-liner setup.
3. You demonstrate the knowledge on how to test the critical parts of the application. We **do not require** 100% coverage.
4. The application is well and logically organised.
5. The submission is accompanied by documentation with the reasoning on the decisions taken.
6. The code is documented and is easy to follow.
7. The answers you provide during code review.
8. An informative, detailed description in the PR.
9. Following the industry standard style guide.
10. A git history (even if brief) with clear, concise commit messages.

---

Good luck!
